package capital.any.service.email;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.email.IEmailDao;
import capital.any.model.base.Email;
import capital.any.service.email.IEmailService;

/**
 * @author Eyal G
 *
 */
@Service("EmailServiceJOB")
public class EmailService extends capital.any.service.base.email.EmailService implements IEmailService {
	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);
	
	@Autowired
	@Qualifier("EmailDaoJOB")
	private IEmailDao emailDao;

	@Override
	public List<Email> selectQueued() {
		return emailDao.selectQueued();
	}

	@Override
	public boolean updateStatus(Email email) {
		return emailDao.updateStatus(email);
	}

	@Override
	public boolean updateSend(Email email) {
		return emailDao.updateSend(email);
	}

	@Override
	public boolean updateRetries(Email email) {
		return emailDao.updateRetries(email);
	}	
}
