package capital.any.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Eyal G
 *
 */
public class MarketAO implements Serializable {
	
	private static final long serialVersionUID = -3202866572631920169L;
	private int id;
	private String name;
	private double closingLevel;
	private Date timeEstCloseing;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the closingLevel
	 */
	public double getClosingLevel() {
		return closingLevel;
	}
	/**
	 * @param closingLevel the closingLevel to set
	 */
	public void setClosingLevel(double closingLevel) {
		this.closingLevel = closingLevel;
	}
	/**
	 * @return the timeEstCloseing
	 */
	public Date getTimeEstCloseing() {
		return timeEstCloseing;
	}
	/**
	 * @param timeEstCloseing the timeEstCloseing to set
	 */
	public void setTimeEstCloseing(Date timeEstCloseing) {
		this.timeEstCloseing = timeEstCloseing;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Market [id=" + id + ", name=" + name + ", closingLevel=" + closingLevel + ", timeEstCloseing="
				+ timeEstCloseing + "]";
	}
}