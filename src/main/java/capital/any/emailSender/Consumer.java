package capital.any.emailSender;

import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.EmailStatus;
import capital.any.base.enums.EmailType;
import capital.any.model.base.Email;
import capital.any.model.base.MailInfo;
import capital.any.service.email.IEmailService;
import capital.any.service.mail.IMailService;
import capital.any.service.sendgrid.ISendgridService;
import capital.any.utils.base.ApplicationContext;

public class Consumer implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(Consumer.class);
	
	private IEmailService emailMailService;
	
	private IMailService mailService;
	private ISendgridService sendgridService;
	
	private BlockingQueue<Email> emailsQueue;
	private String name;
    
    public Consumer(BlockingQueue<Email> emailsQueue, String name) {
        this.emailsQueue = emailsQueue;
        this.name = name;
        emailMailService = ApplicationContext.getApplicationContext().getBean(IEmailService.class);
        mailService = ApplicationContext.getApplicationContext().getBean(IMailService.class);
        sendgridService = ApplicationContext.getApplicationContext().getBean(ISendgridService.class);
    }

	@Override
	public void run() {
		logger.debug("start thread " + name);
		ObjectMapper mapper = new ObjectMapper();
		Email email = null;
		MailInfo mailInfo = null;
		while(true) {
			try {
				logger.debug("thread " + name + " about to take new email");
				email = emailsQueue.take();
				logger.debug("thread " + name + " going to send email " + email);
				mailInfo = mapper.readValue(email.getEmailInfo(), MailInfo.class);
				if (mailInfo.getTypeId() == EmailType.TEMPLATE.getId()) {
					sendgridService.sendMail(mailInfo);
				} else {
					mailService.prepareAndSend(mailInfo);
				}
				logger.debug("thread " + name + " sent email id " + email.getId());
				emailMailService.updateSend(email);
				logger.debug("thread " + name + " sent email and updated " + email.getId());
			} catch (Exception e) {
				logger.debug("thread " + name + " faild to send email id = " + email.getId() + " Retries = " + email.getRetries(), e);
				if (email.getRetries() < 3) {
					logger.debug("thread " + name + " update Retries ");
					email.setRetries(email.getRetries() + 1);
					emailMailService.updateRetries(email);
				} else {
					logger.debug("thread " + name + " update faild ");
					email.setStatusId(EmailStatus.FAILED.getStatusId());
					emailMailService.updateStatus(email);
				}
			}
		}
	}
	
}
