package capital.any.service.sms;

import java.util.List;

import capital.any.model.base.SMS;

/**
 * @author eran.levy
 *
 */
public interface ISMSService extends capital.any.service.base.sms.ISMSService {
	
	/**
	 * Get list of SMS to send
	 * @return
	 */
	public List<SMS> getSMStoSend(int limit); 
	
}
