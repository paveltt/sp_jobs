package capital.any.service.sms;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.sms.ISMSDao;
import capital.any.model.base.SMS;


/**
 * @author eran.levy
 *
 */
@Service("SMSServiceJob")
public class SMSService extends capital.any.service.base.sms.SMSService implements ISMSService{
	
	private static final Logger logger = LoggerFactory.getLogger(SMSService.class);
	
	@Autowired @Qualifier("SMSDaoJob")
	protected ISMSDao SMSDao;
	
	@Override
	public List<SMS> getSMStoSend(int limit) {
		return SMSDao.getSMStoSend(limit);
	}
}
