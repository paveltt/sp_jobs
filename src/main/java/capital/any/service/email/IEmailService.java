package capital.any.service.email;

import java.util.List;

import capital.any.model.base.Email;

/**
 * @author Eyal G
 *
 */
public interface IEmailService extends capital.any.service.base.email.IEmailService {	
	
	/**
	 * select all email in queued 
	 * @return {@link}List<Email>
	 */
	List<Email> selectQueued();

	/**
	 * update email status by id
	 * @param email
	 * @return true if success else false
	 */
	boolean updateStatus(Email email);

	/**
	 * update that email send successful
	 * @param email
	 * @return true if success else false
	 */
	boolean updateSend(Email email);
	
	/**
	 * update that email not send and need to retry
	 * @param email
	 * @return true if success else false
	 */
	boolean updateRetries(Email email);
}
