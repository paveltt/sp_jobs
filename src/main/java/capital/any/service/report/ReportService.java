package capital.any.service.report;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.report.IReportDao;

/**
 * @author Eyal G
 *
 */
@Service
public class ReportService implements IReportService {
	
	private static final Logger logger = LoggerFactory.getLogger(ReportService.class);
	
	@Autowired
	private IReportDao reportDao;

	@Override
	public Map<String, Object> getDailyReport() {
		// TODO Auto-generated method stub
		return reportDao.getDailyReport();
	}
	
	
}
