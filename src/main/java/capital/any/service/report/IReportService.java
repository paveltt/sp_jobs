package capital.any.service.report;

import java.util.Map;

/**
 * @author @author Eyal G
 *
 */
public interface IReportService {
	
	/**
	 * select daily report
	 * @return map of column value
	 */
	Map<String, Object> getDailyReport();
	
}
