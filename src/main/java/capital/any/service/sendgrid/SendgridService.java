package capital.any.service.sendgrid;

import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Personalization;

import capital.any.base.enums.DBParameter;
import capital.any.base.enums.EmailParameters;
import capital.any.model.base.EmailTemplate;
import capital.any.model.base.MailInfo;
import capital.any.model.base.Sendgrid;
import capital.any.service.IUtilService;
import capital.any.service.base.dbParameter.IDBParameterService;
import capital.any.service.base.emailTemplate.IEmailTemplateService;
import capital.any.service.sendgrid.ISendgridService;
import capital.any.service.sendgrid.SendgridService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("SendgridServiceJOB")
public class SendgridService extends capital.any.service.base.sendgrid.SendgridService implements ISendgridService {
	private static final Logger logger = LoggerFactory.getLogger(SendgridService.class);
	
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private IEmailTemplateService emailTemplateService;
	@Value("${spring.mail.username}")
	private String from;
	@Autowired
	private IDBParameterService dbParameterService;
	
	@Override
	public void sendMail(MailInfo mailInfo) throws Exception {
		try {
			EmailTemplate et = new EmailTemplate(mailInfo.getEmailActionId(), mailInfo.getLanguageId());
			EmailTemplate emailTemplate = emailTemplateService.getTemplate(et);
			/* Personalization */
		    Personalization personalization = new Personalization();
		    personalization.addTo(new Email(mailInfo.getTo()));
		    Map<String, String> mapParamValue = mailInfo.getMap();
		    if (null != mapParamValue && !mapParamValue.isEmpty()) {
		    	for (Iterator<String> iter = mapParamValue.keySet().iterator(); iter.hasNext();) {
			    	String param = iter.next();
			    	String value = mapParamValue.get(param);
			    	personalization.addSubstitution(param, value);
			    }
		    }
		    /* Mail */
		    Mail mail = new Mail();
		    mail.setFrom(new Email(from));
		    mail.setTemplateId(emailTemplate.getTemplateId());
		    mail.addPersonalization(personalization);
		    /* Sendgrid */
		    Sendgrid sendgrid = new Sendgrid();
		    sendgrid.setMail(mail);
			sendgrid.setEndpoint("mail/send");
			sendgrid.setBody(sendgrid.getMail().build());
			post(sendgrid);
		} catch (Exception e) {
			logger.error("can't sendMail via Sendgrid Service.", e);
			throw e;
		}
	}
	
	/**
	 * Send Campaign
	 * @param sendgrid
	 */
	@Override
	public void sendCampaign(Sendgrid sendgrid) {
		try {
			sendgrid.setEndpoint("campaigns/" + sendgrid.getCampaignId() + "/schedules/now");
			sendgrid.setBody(IUtilService.EMPTY_STRING);
			post(sendgrid);
		} catch (Exception e) {
			logger.error("can't sendCampaign via Sendgrid Service.", e);
		}
	}
	
	/**
	 * Update Recipients
	 * @param sendgrid
	 */
	@Override
	public void updateRecipients(Sendgrid sendgrid) {
		try {
			sendgrid.setEndpoint("contactdb/recipients");
			sendgrid.setBody(mapper.writeValueAsString(sendgrid.getSendgridRecipientList()));
			patch(sendgrid);
		} catch (JsonProcessingException e) {
			logger.error("can't updateRecipients Sendgrid Service.", e);
		}
	}
	
}
