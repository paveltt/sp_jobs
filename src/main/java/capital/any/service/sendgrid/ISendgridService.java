package capital.any.service.sendgrid;

import capital.any.model.base.MailInfo;
import capital.any.model.base.Sendgrid;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface ISendgridService extends capital.any.service.base.sendgrid.ISendgridService {

	void updateRecipients(Sendgrid sendgrid);

	void sendCampaign(Sendgrid sendgrid);

	void sendMail(MailInfo mailInfo) throws Exception;
}
