package capital.any.service.productCoupon;

import java.util.List;
import capital.any.model.base.ProductCoupon;

/**
 * @author Eyal Goren
 *
 */
public interface IProductCouponService extends capital.any.service.base.productCoupon.IProductCouponService {	
	
	/**
	 * get products coupons that need to check if we should pay or not
	 * @return
	 */
	List<ProductCoupon> getCouponsToPay();

	/**
	 * check if need to pay for this coupon and if need pay to all users that investment on this product
	 * @param productCoupon
	 * @throws Exception 
	 */
	void pay(ProductCoupon productCoupon) throws Exception;
	
	/**
	 * update the product coupon to be mark as processed
	 * @param productCoupon
	 * @return true id we update else false
	 */
	boolean updateIsPaid(ProductCoupon productCoupon);
}
