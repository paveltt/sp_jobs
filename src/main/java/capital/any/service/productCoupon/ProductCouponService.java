package capital.any.service.productCoupon;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.base.enums.ProductHistoryAction;
import capital.any.base.enums.Table;
import capital.any.dao.productCoupon.IProductCouponDao;
import capital.any.model.base.BalanceRequest;
import capital.any.model.base.Investment;
import capital.any.model.base.Product;
import capital.any.model.base.ProductCoupon;
import capital.any.model.base.ProductHistory;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.TransactionReference;
import capital.any.model.base.UserControllerDetails;
import capital.any.model.base.BalanceHistory.BalanceHistoryCommand;
import capital.any.model.base.BalanceRequest.OperationType;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionCoupon;
import capital.any.service.base.IBalanceService;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.base.productHistory.IProductHistoryService;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.transactionOperation.ITransactionOperationService;
import capital.any.service.base.transactionStatus.ITransactionStatusService;
import capital.any.service.investment.IInvestmentService;
import capital.any.service.product.IProductService;

/**
 * @author Eyal Goren
 *
 */
@Service("ProductCouponServiceJob")
public class ProductCouponService extends capital.any.service.base.productCoupon.ProductCouponService implements IProductCouponService {
	private static final Logger logger = LoggerFactory.getLogger(ProductCouponService.class);
	
	@Autowired
	@Qualifier("ProductCouponDaoJob")
	private IProductCouponDao productCouponDao;
	
	@Autowired
	@Qualifier("ProductCouponHCDaoJOB")
	private IProductCouponDao productCouponHCDao;
	
	@Autowired @Qualifier("ProductServiceJob")
	private IProductService productService;
	
	@Autowired
	private IInvestmentService investmentService;
	
	@Autowired
	private ICurrencyService currencyService;
	
	@Autowired
	private ITransactionOperationService transactionOperationService;
	
	@Autowired
	private ITransactionStatusService transactionStatusService;
	
	@Autowired
	private ITransactionService transactionService;
	
	@Autowired
	private IBalanceService balanceService;
	
	@Autowired
	private IServerConfiguration serverConfiguration;
	
	@Autowired
	private IProductHistoryService productHistoryService;
	
	@Autowired
	private ObjectMapper mapper;

	@Override
	public List<ProductCoupon> getCouponsToPay() {
		return productCouponDao.getCouponsToPay();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void pay(ProductCoupon productCoupon) throws Exception {
		logger.debug("start paying coupon " + productCoupon);
		Product product = productService.getProduct(productCoupon.getProductId());
		Date issueDate = product.getIssueDate();
		for (ProductCoupon productCoupon2 : product.getProductCoupons()) {
			if (productCoupon2.getId() == productCoupon.getId()) {
				break;
			}
			issueDate = productCoupon2.getPaymentDate();
		}
		double payRate = productCoupon.getCouponValue(productCoupon.getObservationLevel(),
				new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getStartTradeLevel())),
				issueDate, product.getDayCountConvention(), product.getProductType().getId());
		logger.debug("coupon payRate" + payRate);
		if (payRate > 0) {
			List<Integer> statuses = new ArrayList<Integer>();
			statuses.add(InvestmentStatusEnum.OPEN.getId());
			List<Investment> investments = investmentService.getByStatusesAndProductId(productCoupon.getProductId(), statuses);
			for (Investment investment : investments) {				
					//coupon value in the product currency
					BigDecimal couponValueOriginal = investment.getDenomination().multiply(new BigDecimal(String.valueOf(payRate))).setScale(0, RoundingMode.HALF_UP);
					//coupon value in base currency
					BigDecimal couponValue = couponValueOriginal.divide(new BigDecimal(String.valueOf(currencyService.getRate(product.getCurrencyId()))), 0, RoundingMode.HALF_UP);
					Transaction transaction = new Transaction();
					transaction.setUser(investment.getUser());
					transaction.setAmount(couponValue.longValue());
					transaction.setStatus(transactionStatusService.get().get(TransactionStatusEnum.SUCCEED.getId()));
//					transaction.setIp(deposit.getIp());
					transaction.setOperation(transactionOperationService.get().get(TransactionOperationEnum.CREDIT.getId()));
					transaction.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.INVESTMENT_COUPON.getId()));
					transaction.setRate(currencyService.getRate(investment.getUser().getCurrencyId()));
					transaction.setTimeCreated(new Date());
					
					TransactionCoupon transactionCoupon = new TransactionCoupon();
					transactionCoupon.setAmonut(couponValueOriginal.longValue());
					transactionCoupon.setProductCouponId(productCoupon.getId());
					transaction.setCoupon(transactionCoupon);
					
					TransactionReference transactionReference = new TransactionReference();
					transactionReference.setReferenceId(investment.getId());
					transactionReference.setTableId(Table.INVESTMENTS.getId());
					transaction.setReference(transactionReference);
					
					TransactionHistory transactionHistory = new TransactionHistory();
					transactionHistory.setActionSource(ActionSource.JOB);
					transactionHistory.setServerName(serverConfiguration.getServerName());
					transactionHistory.setStatus(transaction.getStatus());
					transactionHistory.setTransactionId(transaction.getId());
					transactionService.insertTransaction(transaction, transactionHistory);
					
					BalanceRequest br = new BalanceRequest();
					br.setAmount(transaction.getAmount());
					br.setCommandHistory(BalanceHistoryCommand.SUCCESS_TRANSACTION);
					br.setOperationType(OperationType.CREDIT);
					br.setReferenceId(transaction.getId());
					br.setUser(investment.getUser());
					br.setUserControllerDetails(new UserControllerDetails(transactionHistory.getActionSource()
								,transaction.getIp()
								,transactionHistory.getLoginId()));				
					balanceService.changeBalance(br);
			};
		}
		productCoupon.setPaid(true);
		updateIsPaid(productCoupon);
		ObjectNode json = mapper.createObjectNode();
		json.put("payRate", payRate);
		json.put("product_coupon_id", productCoupon.getId());
		ProductHistory productHistory = new ProductHistory(productCoupon.getProductId(),
				new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_PRODUCT_COUPON_PAID.getId()),
				json.toString(), ActionSource.JOB, 0,
				serverConfiguration.getServerName());
		productHistoryService.insert(productHistory);
	}

	@Override
	public boolean updateIsPaid(ProductCoupon productCoupon) {
		boolean result = productCouponDao.updateIsPaid(productCoupon);
		if (result) {
			result = productCouponHCDao.updateIsPaid(productCoupon);
		}
		return result;
	}
	
	
}
