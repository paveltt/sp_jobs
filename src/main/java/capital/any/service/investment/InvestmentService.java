package capital.any.service.investment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentHistory;
import capital.any.model.base.InvestmentStatus;
import capital.any.service.investment.IInvestmentService;

/**
 * @author Eyal G
 *
 */
@Service("InvestmentServiceJOB")
public class InvestmentService extends capital.any.service.base.investment.InvestmentService implements IInvestmentService {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentService.class);


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void cancelPendingInvestments(long productId) {
		logger.info("Canceling Pending Investments for product id " + productId);
		List<Integer> statuses = new ArrayList<Integer>();
		statuses.add(InvestmentStatusEnum.PENDING.getId());
		List<Investment> investments = getByStatusesAndProductId(productId, statuses);
		logger.debug("Found " + investments.size() + " investments for product id " + productId);
		investments.forEach(investment -> {
			investment.setInvestmentStatus(new InvestmentStatus(InvestmentStatusEnum.CANCELED_INSUFFICIENT_FUNDS.getId()));
			InvestmentHistory investmentHistory = new InvestmentHistory();
			investmentHistory.setActionSource(ActionSource.JOB);
			investmentHistory.setInvestmentStatus(investment.getInvestmentStatus());
			investmentHistory.setTimeCreated(new Date());
			investmentHistory.setInvestmntId(investment.getId());
			investmentHistory.setServerName(serverConfiguration.getServerName());
			investment.setInvestmentHistory(investmentHistory);
			updateStatus(investment);
		});
	}
}
