package capital.any.service.mail;

import org.springframework.mail.MailException;

import capital.any.model.base.MailInfo;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMailService {
	
	/**
	 * send email with template
	 * @param mailInfo
	 * @throws MailException 
	 */
	void prepareAndSend(MailInfo mailInfo) throws MailException;
	
//	/**
//	 * send email without template
//	 * @param mailInfo
//	 */
//	void prepareAndSendNoTemplate(MailInfo mailInfo, boolean isHTML);
	
}
