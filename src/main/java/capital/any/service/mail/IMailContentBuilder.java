package capital.any.service.mail;

import capital.any.model.base.MailInfo;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMailContentBuilder {	
	String build(MailInfo mailInfo);	
}
