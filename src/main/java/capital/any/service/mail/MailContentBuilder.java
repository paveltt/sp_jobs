package capital.any.service.mail;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templateresolver.TemplateResolver;

import capital.any.model.base.MailInfo;
import capital.any.service.base.language.ILanguageService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class MailContentBuilder implements IMailContentBuilder {
	private static final Logger logger = LoggerFactory.getLogger(MailContentBuilder.class);
	
	@Autowired
	private SpringTemplateEngine templateEngine;
	
	@Autowired
	private ILanguageService languageService;
	
	@PostConstruct
	private void init() {
		TemplateResolver resolver = new TemplateResolver();
        resolver.setResourceResolver(new AwsTemplateResolver());
        resolver.setSuffix(".html");
        resolver.setTemplateMode(TemplateResolver.DEFAULT_TEMPLATE_MODE);
        resolver.setCharacterEncoding("UTF-8");
        resolver.setOrder(1);
        templateEngine.setTemplateResolver(resolver);
	}

	@Override
	public String build(MailInfo mailInfo) {
		Context context = new Context();
		context.setVariables(mailInfo.getMap());
		String languageCode = "en";
		try {
			languageCode = languageService.get().get(mailInfo.getLanguageId()).getCode();
		} catch (Exception e) {
			logger.error("cant get language code for language id " + mailInfo.getLanguageId() + " using defualt en");
		}
		String awsKey = languageCode + "/" + mailInfo.getTemplateName();
        return templateEngine.process(awsKey, context);
	}
}
