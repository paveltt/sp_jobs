package capital.any.service.mail;

import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.TemplateProcessingParameters;
import org.thymeleaf.resourceresolver.IResourceResolver;

import capital.any.service.base.aws.IAwsService;
import capital.any.service.mail.AwsTemplateResolver;
import capital.any.utils.base.ApplicationContext;

/**
 * get templates from AWS
 * @author eyal.goren
 *
 */
public class AwsTemplateResolver implements IResourceResolver {
	private static final Logger logger = LoggerFactory.getLogger(AwsTemplateResolver.class);
	
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public InputStream getResourceAsStream(TemplateProcessingParameters templateProcessingParameters,
			String resourceName) {
		if (resourceName == null || resourceName.isEmpty()) {
			throw new IllegalArgumentException("Resource name cannot be null");
		}
		try {
			IAwsService awsService = ApplicationContext.getApplicationContext().getBean(IAwsService.class);
			InputStream template = awsService.getTemplate(resourceName);
			return template;
		} catch (Exception e) {
			throw new RuntimeException("Cannot open resource '" + resourceName + "'", e);
		}
	}
}
