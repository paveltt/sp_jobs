package capital.any.service.product;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.base.enums.EmailAction;
import capital.any.base.enums.EmailGroup;
import capital.any.base.enums.EmailType;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.dao.product.IProductDao;
import capital.any.model.base.Email;
import capital.any.model.base.MailInfo;
import capital.any.model.base.Product;
import capital.any.model.base.ProductHistory;
import capital.any.service.base.email.IEmailService;
import capital.any.service.base.emailGroup.IEmailGroupService;
import capital.any.service.investment.IInvestmentService;


/**
 * @author EyalG
 *
 */
@Service("ProductServiceJob")
public class ProductService extends capital.any.service.base.product.ProductService implements IProductService {
	private static final Logger logger = LoggerFactory.getLogger(ProductService.class);	
	
	@Autowired @Qualifier("ProductDaoJob")
	protected IProductDao productDao;
	
	@Autowired
	@Qualifier("InvestmentServiceJOB")
	private IInvestmentService investmentService;
	
	@Autowired
	private IEmailGroupService emailGroupService;
	
	@Autowired
	private IEmailService emailService;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Override
	public List<Product> getProductsStatusSecondary() {
		return productDao.getProductsStatusSecondary();
	}

	@Override
	public List<Long> getProductsForSettle() {
		return productDao.getProductsForSettle();
	}

	@Override
	public List<Product> getProductsStatusWaitingForSettle() {
		return productDao.getProductsStatusWaitingForSettle();		
	}

	@Override
	public void changeProductStatus() throws Exception {
		StringBuilder email = new StringBuilder();
		email.append("<html><head><style type='text/css'>table, th, td {border: 1px solid black;}"
				+ "</style></head>"
                + "<body>");
		boolean shouldSend = false;
		List<Product> productsStatusSecondary = getProductsStatusSecondary();
		if (!productsStatusSecondary.isEmpty()) {
			email.append("<b>product that move to status secondary</b>"
		                + "<table class='out'><span style=border-color: black, border-width: 1px>"
		                + "<tr><td>product id</td><td>ask</td><td>bid</td></tr>");
			shouldSend = true;
		}
		for (Product product : productsStatusSecondary) {
			logger.info("change status to seconday Products " + product.getId());
			updateStatus(product.getId(), ProductStatusEnum.SECONDARY, new ProductHistory(ActionSource.JOB));
			investmentService.cancelPendingInvestments(product.getId());
			try {
				email.append("<tr><td>" + product.getId() + "</td><td>" + product.getAsk() + "</td><td>" + product.getBid() + "</td></tr>");
			} catch (Exception e) {
				logger.error("Cant add row to email " + product.getId());
			}
		}
		if (shouldSend) {
			email.append("</table>");
		}
		List<Product> productsStatusWaitingForSettle = getProductsStatusWaitingForSettle();
		if (!productsStatusWaitingForSettle.isEmpty()) {
			email.append("<b>product that move to status Waiting For Settle</b>"
					+ "<table class='out'><span style=border-color: black, border-width: 1px>"
					+ "<tr><td>product id</td><td>ask</td><td>bid</td></tr>");
			shouldSend = true;
		}
		for (Product product : productsStatusWaitingForSettle) {
			logger.info("change status to waiting for settle Products " + product.getId());
			updateStatus(product.getId(), ProductStatusEnum.WAITING_FOR_SETTLE, new ProductHistory(ActionSource.JOB));
			try {
				email.append("<tr><td>" + product.getId() + "</td><td>" + product.getAsk() + "</td><td>" + product.getBid() + "</td></tr>");
			} catch (Exception e) {
				logger.error("Cant add row to email " + product.getId());
			}
		}
		if (!productsStatusWaitingForSettle.isEmpty()) {
			email.append("</table>");
		}
		email.append("</body></html>");
		if (shouldSend) {
			MailInfo mailInfo = new MailInfo(emailGroupService.get(EmailGroup.TRADERS.getId()), "products status change", email.toString(), EmailType.NO_TEMPLATE_TEXT_HTML.getId(), EmailAction.CHANGE_PRODUCT_STATUS.getId());
			try {
				Email emailToSend = new Email();		
				emailToSend.setActionSource(ActionSource.JOB);
				emailToSend.setEmailInfo(mapper.writeValueAsString(mailInfo));
				emailService.insert(emailToSend);
			} catch (JsonProcessingException e) {
				logger.error("cant send email", e);
			}
		}
	}

}
