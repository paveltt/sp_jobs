package capital.any.service.product;

import java.util.List;

import capital.any.model.base.Product;

/**
 * @author EyalG
 *
 */
public interface IProductService extends capital.any.service.base.product.IProductService {	
	
	/**
	 * update all products that
	 * correct status is subscription 
	 * and we are after subscription_End_Date
	 * and we are before last_trading_date
	 * to statues secondary
	 */
	List<Product> getProductsStatusSecondary();
	
	/**
	 * get all products id that need to be settled
	 * @return list of products id to settle
	 */
	List<Long> getProductsForSettle();

	/**
	 * update all products that
	 * correct status is subscription 
	 * and we are after subscription_End_Date
	 * and we are before last_trading_date
	 * to statues primary
	 */
	List<Product> getProductsStatusWaitingForSettle();
	
	void changeProductStatus() throws Exception;
	
}
