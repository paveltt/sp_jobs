package capital.any.service.market;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.market.IMarketDao;
import capital.any.model.MarketAO;

/**
 * @author Eyal G
 *
 */
@Service("marketServiceJOB")
public class MarketService extends capital.any.service.base.market.MarketService implements IMarketService {
	
	private static final Logger logger = LoggerFactory.getLogger(MarketService.class);
	
	@Autowired
	@Qualifier("MarketDaoJOB")
	private IMarketDao marketDao;
	
	@Override
	public List<MarketAO> getHistoryAO(int startDay, int endDay, Integer marketId) {
		return marketDao.getHistoryAO(startDay, endDay, marketId);
	}

	@Override
	public List<MarketAO> getHistoryAOYearly(Integer marketId) {
		return marketDao.getHistoryAOYearly(marketId);
	}
}
