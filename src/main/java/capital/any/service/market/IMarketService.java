package capital.any.service.market;

import java.util.List;

import capital.any.model.MarketAO;

/**
 * @author @author Eyal G
 *
 */
public interface IMarketService extends capital.any.service.base.market.IMarketService {
	
	/**
	 * get history from ao for specific market and days
	 * @param startDay how many days to reduce from today (365 for 1 year)
	 * @param endDay how many days to reduce from today (365 for 1 year)
	 * @param marketId market id in AO
	 */
	List<MarketAO> getHistoryAO(int startDay, int endDay, Integer marketId);
	
	/**
	 * get history of last year
	 * @param marketId
	 * @return list of daily closing level of last year
	 */
	List<MarketAO> getHistoryAOYearly(Integer marketId);
}
