package capital.any.job;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import capital.any.base.enums.DBParameter;
import capital.any.base.enums.EmailStatus;
import capital.any.emailSender.Consumer;
import capital.any.model.base.Email;
import capital.any.service.base.dbParameter.IDBParameterService;
import capital.any.service.email.IEmailService;

/**
 * @author Eyal Goren
 *
 */
@Component
public class EmailSenderJob implements IJob {
	private static final Logger logger = LoggerFactory.getLogger(EmailSenderJob.class);

	@Autowired
	private IEmailService emailMailService;
	
	@Autowired
	private IDBParameterService dbParameterService;
	
	private ArrayList<Thread> consumers;
	BlockingQueue<Email> emailsQueue;
	
	public void run() {
		logger.info("EmailSenderJob start");
		Long consumersSize = dbParameterService.get(DBParameter.EMAIL_SENDER_CONSUMERS.getId()).getNumValue();
		int queueSize = Integer.valueOf(String.valueOf(dbParameterService.get(DBParameter.EMAIL_SENDER_QUEUE_SIZE.getId()).getNumValue()));
		emailsQueue = new ArrayBlockingQueue<Email>(queueSize);
		
		consumers = new ArrayList<Thread>();
		for (int i = 0; i < consumersSize; i++) {
			Thread consumer = new Thread(new Consumer(emailsQueue, "Consumer " + (i + 1)));
			consumers.add(consumer);
			consumer.start();
		}        
        
        while(true) {
			List<Email> emailQueuedTemp = emailMailService.selectQueued(); //TODO add limit
			logger.info("found " + emailQueuedTemp.size() + " new emails");
			for (Email email : emailQueuedTemp) {
				try {
					logger.info("about to update email id " + email.getId());
					email.setStatusId(EmailStatus.PROCESSING.getStatusId());
					emailMailService.updateStatus(email);
					logger.info("about to insert email id " + email.getId());
					emailsQueue.put(email);
					logger.info("done insert email id " + email.getId());
				} catch (InterruptedException e) {
					logger.error("cant insert email to queue id = " + email.getId(), e);
				} 
			}
			
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				logger.error("cant sleep", e);
			}
		}
	}

	@Override
	public void stop() {
		logger.debug("EmailSenderJob stop 1");
		for (Thread thread : consumers) {
			thread.interrupt();
		}
		logger.debug("EmailSenderJob stop 2");
	}
}
