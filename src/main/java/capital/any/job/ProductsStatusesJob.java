package capital.any.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import capital.any.service.product.IProductService;

/**
 * @author Eyal Goren
 *
 */
@Component
public class ProductsStatusesJob implements IJob {
	private static final Logger logger = LoggerFactory.getLogger(ProductsStatusesJob.class);
	
	@Autowired
	@Qualifier("ProductServiceJob")
	private IProductService productService;	
	
	public void run() throws Exception {
		logger.info("Method update Products Statuses start");
		productService.changeProductStatus();
		logger.info("Method update Products Statuses end");
	}

	@Override
	public void stop() {
		logger.info("ProductsStatusesJob stop");			
	}
		
}