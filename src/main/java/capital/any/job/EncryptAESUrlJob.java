package capital.any.job;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.base.security.ISecureAlgorithmsService;
import capital.any.service.base.csv.ICSVService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class EncryptAESUrlJob implements IJob {
	private static final Logger logger = LoggerFactory.getLogger(EncryptAESUrlJob.class);
	
	@Autowired
	private ICSVService csvService;
	
	@Autowired
	private ISecureAlgorithmsService secureAlgorithmsService; 

	@Override
	public void run() {
		String csvFilePath = "D:/users_info"; 
		String csvFileType = ".csv";
		String csvFileEncrypted = "_encrypted";
		String csvColumnHeader = "Hash_";
		List<String> columnsForEncrypt = new ArrayList<String>();
		columnsForEncrypt.add("Email");
		List<List<String>> docInfo = csvService.readDocument(csvFilePath + csvFileType);
		try {
			docInfo = secureAlgorithmsService.encryptAESUrl(docInfo, columnsForEncrypt, csvColumnHeader);
			csvService.writeDocument(csvFilePath + csvFileEncrypted + csvFileType, docInfo);
		} catch (Exception e) {
			logger.error("Problem with EncryptAESUrlJob.", e);
		}
	}

	@Override
	public void stop() {
		logger.debug("EncryptAESUrlJob stop");
	}

}
