package capital.any.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import capital.any.base.enums.ActionSource;
import capital.any.model.base.ProductHistory;
import capital.any.service.product.IProductService;

/**
 * @author eyal goren
 *
 */
@Component
public class ProductsSettlesJob implements IJob {
	private static final Logger logger = LoggerFactory.getLogger(ProductsSettlesJob.class);
	
	@Autowired
	@Qualifier("ProductServiceJob")
	private IProductService productService;
	
	public void run() {
		logger.info("Method settle Products start");		
		for (Long productId : productService.getProductsForSettle()) {
			try {
				productService.settle(productId, new ProductHistory(ActionSource.JOB));
			} catch (Exception e) {
				logger.error("Can't settle product id " + productId, e);
			}
		}
		logger.info("Method settle Products end");
	}

	@Override
	public void stop() {
		logger.info("ProductsSettlesJob stop");		
	}

}