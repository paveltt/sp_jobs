package capital.any.job;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.base.enums.MarketPriceTypeEnum;
import capital.any.model.MarketAO;
import capital.any.model.base.Market;
import capital.any.model.base.MarketPrice;
import capital.any.service.base.marketPrice.IMarketPriceService;
import capital.any.service.market.IMarketService;

/**
 * @author eranl
 *
 */
@Service
public class MarketHistoryJob {
	private static final Logger logger = LoggerFactory.getLogger(MarketJob.class);
	
	@Autowired
	private IMarketService marketService;

	@Autowired
	private IMarketPriceService marketPriceService;

/*
 * get history from yahoo
 */
//	public void  run() {	
//		logger.info("Method MarketHistoryJob start");
//		Calendar from = Calendar.getInstance();
//		Calendar to = Calendar.getInstance();
//		from.add(Calendar.YEAR, -1);
//		
//		HashMap<String, Market> markets = marketService.getMarketsHMByFeedName();
//		String[] symbolList = new String[markets.size()]; // converting the
//															// arrayList to List
//		List<String> list = markets.entrySet().stream().map(m -> m.getValue().getFeedName())
//				.collect(Collectors.toList());
//
//		String[] symbolList1 = list.toArray(symbolList);
//
//		Map<String, Stock> stocks = new HashMap<String, Stock>();
//		for (String assetSymbol: symbolList1) {
//		//String assetSymbol = "OCEU.EX";
//			try {				
//				Stock stock = YahooFinance.get(assetSymbol, from, to, Interval.DAILY);
//				stocks.put(assetSymbol, stock);
//			} catch (IOException e) {
//					logger.error("Error to get market from Yahoo finance " + assetSymbol, e);
//			}
//		}
//		for (Stock stock : stocks.values()) {
//			Market market = markets.get(stock.getSymbol());
//			ArrayList<MarketPrice> marketPriceList = new ArrayList<MarketPrice>();
//			List<HistoricalQuote> stockHistQuotes = null;
//			try {
//				stockHistQuotes = stocks.get(stock.getSymbol()).getHistory();
//			} catch (Exception e) {				
//				logger.error("Error to get market from Yahoo finance " +stock.getSymbol(), e);
//			}
//			for (HistoricalQuote quote : stockHistQuotes) {
//				BigDecimal price = quote.getClose();
//				if (price == null) {
//					continue;
//				}
//				MarketPrice marketPrice = new MarketPrice();
//				marketPrice.setMarketId(market.getId());
//				marketPrice.setPrice(price.doubleValue());
//				marketPrice.setTypeId(MarketPriceTypeEnum.HISTORY_PRICE.getId());
//				marketPrice.setPriceDate(quote.getDate().getTime());
//				marketPriceList.add(marketPrice);
//			}
//			marketPriceService.insertMarketPriceBatch(marketPriceList);			
//		}
//		logger.info("Method MarketHistoryJob end");
//	}
	
	/**
	 * get history from AO
	 */
	public void  run() {	
		logger.info("Method MarketHistoryJob start");
		//get ac markets
		Map<Integer, Market> markets = marketService.getThin();
		List<String> noHiostory = new ArrayList<String>();
		markets.forEach((k, v) -> {
			List<MarketAO> historyAO = marketService.getHistoryAOYearly(v.getMarketIdAo());
			if (!historyAO.isEmpty()) {
				ArrayList<MarketPrice> marketPriceList = new ArrayList<MarketPrice>();
				historyAO.forEach(history -> {
					MarketPrice marketPrice = new MarketPrice();
					marketPrice.setMarketId(v.getId());
					marketPrice.setPrice(history.getClosingLevel());
					marketPrice.setTypeId(MarketPriceTypeEnum.HISTORY_PRICE.getId());
					marketPrice.setPriceDate(history.getTimeEstCloseing());
					marketPriceList.add(marketPrice);
				});
				try {
					marketPriceService.insertMarketHistoryPrice(marketPriceList);
				} catch (Exception e) {
					logger.error("cant insert history for marketPrice " + marketPriceList);
					noHiostory.add(v.getName());
				}
			} else {
				noHiostory.add(v.getName());
			}
		}); 
		
		logger.info("cant update this symboles " + noHiostory); 
		logger.info("number of markets: " + markets.size() + " cant update " + noHiostory.size());
		logger.info("Method MarketHistoryJob end");
	}
					
}	