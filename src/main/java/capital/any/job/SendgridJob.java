package capital.any.job;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import capital.any.model.base.Sendgrid;
import capital.any.model.base.SendgridRecipient;
import capital.any.model.base.User;
import capital.any.model.base.UserFilters;
import capital.any.service.base.user.IUserService;
import capital.any.service.sendgrid.ISendgridService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class SendgridJob implements IJob {
	private static final Logger logger = LoggerFactory.getLogger(SendgridJob.class);
	
	@Autowired
	private IUserService userService;
	@Autowired
	private ISendgridService sendgridService;
	
	@Override
	public void run() {
		logger.info("SendgridJob start.");
		LocalDateTime today = LocalDateTime.now();
		LocalDateTime yesterday = today.minusDays(7);
		UserFilters userFilters = new UserFilters();
		userFilters.setFromDate(yesterday);
		userFilters.setToDate(today);
		logger.info("About to get users, " + userFilters.toString());
		List<User> map = userService.getUsersByDates(userFilters);
		List<SendgridRecipient> sendgridRecipientList = new ArrayList<SendgridRecipient>(); 
		for (User u : map) {
			SendgridRecipient sendgridRecipient = new SendgridRecipient();
			sendgridRecipient.setEmail(u.getEmail());
			sendgridRecipient.setAcUserId(u.getId());
			sendgridRecipientList.add(sendgridRecipient);
		}
		Sendgrid sendgrid = new Sendgrid();
		sendgrid.setSendgridRecipientList(sendgridRecipientList);
		sendgridService.updateRecipients(sendgrid);
	}

	@Override
	public void stop() {
		logger.info("SendgridJob end.");
	}
}
