package capital.any.job;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import capital.any.base.enums.MarketPriceTypeEnum;
import capital.any.model.base.Market;
import capital.any.model.base.MarketPrice;
import capital.any.service.base.market.IMarketService;
import capital.any.service.base.marketPrice.IMarketPriceService;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;

/**
 * @author eranl
 *
 */
@Component
public class MarketJob implements IJob {
	private static final Logger logger = LoggerFactory.getLogger(MarketJob.class);

	@Autowired
	private IMarketService marketService;

	@Autowired
	private IMarketPriceService marketPriceService;

	public void run() throws Exception {
		logger.info("Method updateMarketsLevel start");
		HashMap<String, Market> markets = marketService.getMarketsHMByFeedName();
		String[] symbolList = new String[markets.size()]; // converting the
															// arrayList to List
		List<String> list = markets.entrySet().stream().map(m -> m.getValue().getFeedName())
				.collect(Collectors.toList());

		String[] symbolList1 = list.toArray(symbolList);

		Map<String, Stock> stocks = null;
		try {
			stocks = (Map<String, Stock>) YahooFinance.get(symbolList1);
		} catch (IOException e) {
				logger.error("Error to get market from Yahoo finance", e);
		}
		for (Iterator<String> iter = markets.keySet().iterator(); iter.hasNext();) {
			String stockTicker = iter.next();
			Market market = markets.get(stockTicker);
			BigDecimal price = stocks.get(stockTicker).getQuote().getPrice();
			if (price == null) {
				continue;
			}
			MarketPrice marketPrice = new MarketPrice();
			marketPrice.setMarketId(market.getId());
			marketPrice.setPrice(price.doubleValue());
			marketPrice.setTypeId(MarketPriceTypeEnum.LAST_PRICE.getId());
			marketPriceService.updateMarketPriceAndProducts(marketPrice);
		}

		logger.info("Method updateMarketsLevel end");
	}

	@Override
	public void stop() {
		logger.debug("UpdateMarketsLevel stop");
	}
}
