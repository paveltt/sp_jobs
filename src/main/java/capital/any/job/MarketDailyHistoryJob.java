package capital.any.job;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.base.enums.EmailAction;
import capital.any.base.enums.EmailType;
import capital.any.base.enums.MarketPriceTypeEnum;
import capital.any.model.base.Email;
import capital.any.model.base.MailInfo;
import capital.any.model.base.Market;
import capital.any.model.base.MarketPrice;
import capital.any.service.base.email.IEmailService;
import capital.any.service.base.marketPrice.IMarketPriceService;
import capital.any.service.market.IMarketService;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

/**
 * @author Eyal Goren
 *
 */
@Service
public class MarketDailyHistoryJob implements IJob {
	private static final Logger logger = LoggerFactory.getLogger(MarketDailyHistoryJob.class);
	
	@Autowired
	private IMarketService marketService;

	@Autowired
	private IMarketPriceService marketPriceService;
	
	@Autowired
	private IEmailService emailService;
	
	@Autowired
	private ObjectMapper mapper;

	/**
	 * get history from yahoo
	 */
	public void  run() {	
		logger.info("Method MarketDailyHistoryJob start");
		Calendar from = Calendar.getInstance();
		Calendar to = Calendar.getInstance();
		from.add(Calendar.DAY_OF_MONTH, -1);
		
		HashMap<String, Market> markets = marketService.getMarketsHMByFeedName();

		String[] symbolList1 = markets.entrySet().stream()
//				.filter(market -> market.getValue().getMarketHistoryProviderId() == MarketHistoryProvider.Yahoo.getId())
				.map(m -> m.getValue().getFeedName())
				.toArray(size -> new String[size]);

		Map<String, List<HistoricalQuote>> historicalQuoteList = new HashMap<String, List<HistoricalQuote>>();
		List<String> noHiostory = new ArrayList<String>();
		for (String assetSymbol: symbolList1) {
		//String assetSymbol = "OCEU.EX";
			try {	
				
				Stock stock = YahooFinance.get(assetSymbol);
				List<HistoricalQuote> stockList = stock.getHistory(from, to, Interval.DAILY);
				historicalQuoteList.put(assetSymbol, stockList); 
			} catch (IOException e) {
					logger.error("Error to get markets", e);
					noHiostory.add(assetSymbol);
			}
		}
		
		for (String stock : historicalQuoteList.keySet()) {
			Market market = markets.get(stock);
			ArrayList<MarketPrice> marketPriceList = new ArrayList<MarketPrice>();			
			for (HistoricalQuote quote : historicalQuoteList.get(stock)) {
				BigDecimal price = quote.getClose();
				if (price == null) {
					continue;
				}
				MarketPrice marketPrice = new MarketPrice();
				marketPrice.setMarketId(market.getId());
				marketPrice.setPrice(price.doubleValue());
				marketPrice.setTypeId(MarketPriceTypeEnum.HISTORY_PRICE.getId());
				marketPrice.setPriceDate(quote.getDate().getTime());
				marketPriceList.add(marketPrice);
			}
			logger.debug("marketPrice " + marketPriceList);
			if (marketPriceList.isEmpty()) {
				noHiostory.add(stock);
			} else {
				try {
					marketPriceService.insertMarketHistoryPrice(marketPriceList);
				} catch (Exception e) {
					logger.error("cant insert history for marketPrice " + marketPriceList);
				}
			}
		}

		logger.info("cant update this symboles " + noHiostory); 
		logger.info("number of markets: " + symbolList1.length + " cant update " + noHiostory.size());
		
		String emailBody = "number of markets: " + markets.size() + " cant update " + noHiostory.size() + ".  cant update this symboles " + noHiostory;		
		MailInfo mailInfo = new MailInfo("eyal.goren@anycapital.com", "update market daily History job report", emailBody, 
				EmailType.NO_TEMPLATE_TEXT.getId(), EmailAction.MARKET_DAILY_HISTORY_JOB.getId());
			
		try {
			Email email = new Email();		
			email.setActionSource(ActionSource.JOB);
			email.setEmailInfo(mapper.writeValueAsString(mailInfo));
			emailService.insert(email);
		} catch (JsonProcessingException e) {
			logger.error("cant send email", e);
		}
		logger.info("Method MarketDailyHistoryJob end");
	}
	
//	/**
//	 * get history from AO
//	 */
//	public void run() {	
//		logger.info("Method MarketDailyHistoryJob start");
//		int startday = 1;
//		int endday = 0;
//		//get ac markets
//		Map<Integer, Market> markets = marketService.getThin();
//		List<String> noHiostory = new ArrayList<String>();
//		markets.forEach((k, v) -> {
//			List<MarketAO> historyAO = marketService.getHistoryAO(startday, endday, v.getMarketIdAo());
//			if (!historyAO.isEmpty()) {
//				ArrayList<MarketPrice> marketPriceList = new ArrayList<MarketPrice>();
//				historyAO.forEach(history -> {
//					MarketPrice marketPrice = new MarketPrice();
//					marketPrice.setMarketId(v.getId());
//					marketPrice.setPrice(history.getClosingLevel());
//					marketPrice.setTypeId(MarketPriceTypeEnum.HISTORY_PRICE.getId());
//					marketPrice.setPriceDate(history.getTimeEstCloseing());
//					marketPriceList.add(marketPrice);
//				});
//				try {
//					marketPriceService.insertMarketHistoryPrice(marketPriceList);
//				} catch (Exception e) {
//					logger.error("cant insert history for marketPrice " + marketPriceList, e);
//					noHiostory.add(v.getName());
//				}
//			} else {
//				noHiostory.add(v.getName());
//			}
//		}); 
//		
//		logger.info("cant update this symboles " + noHiostory); 
//		logger.info("number of markets: " + markets.size() + " cant update " + noHiostory.size());
//		
//		String emailBody = "number of markets: " + markets.size() + " cant update " + noHiostory.size() + ".  cant update this symboles " + noHiostory;		
//		MailInfo mailInfo = new MailInfo("eyal.goren@anycapital.com", "update market daily History job report", emailBody, 
//				EmailType.NO_TEMPLATE_TEXT.getId(), EmailAction.MARKET_DAILY_HISTORY_JOB.getId());
//		try {
//			Email email = new Email();		
//			email.setActionSource(ActionSource.JOB);
//			email.setEmailInfo(mapper.writeValueAsString(mailInfo));
//			emailService.insert(email);
//		} catch (JsonProcessingException e) {
//			logger.error("cant send email", e);
//		}
//		logger.info("Method MarketDailyHistoryJob end");
//	}

	@Override
	public void stop() {
		logger.info("MarketDailyHistoryJob stop");
	}
					
}	