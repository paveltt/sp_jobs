//package capital.any.job;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import capital.any.model.base.MailInfo;
//import capital.any.service.mail.IMailContentBuilder;
//import capital.any.service.mail.IMailService;
//
///**
// * 
// * @author eyal.ohana
// *
// */
//@Service
//public class MailJob implements IJob {
//	private static final Logger logger = LoggerFactory.getLogger(MailJob.class);
//	
//	@Autowired
//	private IMailService mailService ;
//
//	@Override
//	public void run() {
//		logger.debug("MailJob start");
//		/* Content's parameter */
//		Map<String, String> map = new HashMap<String, String>();
//		map.put(IMailContentBuilder.PARAM_FIRST_NAME, "anycapital");
//		/* Mail Info */
//		MailInfo mailInfo = new MailInfo();
//		mailInfo.setFrom("support.test@anycapital.com");
//		mailInfo.setTo("eyal.ohana@anycapital.com");
//		mailInfo.setSubject("Test AC");
//		mailInfo.setTemplateName("mailTemplateTest");
//		mailInfo.setMap(map);
//		/* Prepare and send */
//		mailService.prepareAndSend(mailInfo);
//		
//	}
//
//	@Override
//	public void stop() {
//		logger.debug("MailJob stop");
//	}
//
//}
