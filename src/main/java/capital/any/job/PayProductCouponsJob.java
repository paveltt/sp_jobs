package capital.any.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import capital.any.service.productCoupon.IProductCouponService;

/**
 * @author Eyal Goren
 *
 */
@Component
public class PayProductCouponsJob implements IJob {
	private static final Logger logger = LoggerFactory.getLogger(PayProductCouponsJob.class);
	
	@Autowired @Qualifier("ProductCouponServiceJob")
	private IProductCouponService productCouponService;
	
	public void run() {
		logger.info("Method Pay Product Coupons start");	
		productCouponService.getCouponsToPay().forEach(productCoupon -> {
			try {
				productCouponService.pay(productCoupon);
			} catch (Exception e) {
				logger.error("Can't pay coupon " + productCoupon, e);
			}
		});
		logger.info("Method Pay Product Coupons end");
	}

	@Override
	public void stop() {
		logger.info("ProductsSettlesJob stop");		
	}

}