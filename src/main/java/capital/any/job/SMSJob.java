package capital.any.job;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import capital.any.model.base.SMS;
import capital.any.model.base.SMSProvider;
import capital.any.service.sms.ISMSService;

/**
 * @author eran.levy
 *
 */
@Component
public class SMSJob implements IJob, ApplicationContextAware {
	
	private static final Logger logger = LoggerFactory.getLogger(SMSJob.class);
	
	private static final int LIMIT = 1000;
	
	@Autowired
	@Qualifier("SMSServiceJob")
	private ISMSService SMSService;
		
	private HashMap<Integer, SMSProvider> smsProviders;
	
	private SMSProvider smsprovider;
		
	public synchronized void run() {
		logger.info("SMSJob start");				
		List<SMS> list = SMSService.getSMStoSend(LIMIT);	
		smsprovider = smsProviders.get(SMSProvider.Provider.MOBIVATE.getToken());
		for (SMS sms : list) {			
			try {
				sms.setProviderId(SMSProvider.Provider.MOBIVATE.getToken());
				smsprovider.send(sms);
				if (sms.getStatus() == SMS.Status.NOT_VERIFIED) {
					sms.setStatus(SMS.Status.WAITING_HLR);
				} else  if (sms.getStatus() == SMS.Status.QUEUED) {
					sms.setStatus(SMS.Status.SUBMITTED);
					sms.setTimeSent(new Date());
				} else {
					sms.setStatus(SMS.Status.FAILED);
				}											
			} catch (Exception e) {
				sms.setStatus(SMS.Status.FAILED);
				logger.error("Cant send or update sms:", e);
			}				
			SMSService.update(sms);
		}
		logger.info("SMSJob end");
	}
	
	@Override
	public void stop() {
		logger.debug("SMSJob stop");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {		
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		smsProviders = (HashMap<Integer, SMSProvider>) SMSService.getSMSProviders();
		for (SMSProvider provider : smsProviders.values()) {
			try {				
				beanFactory.autowireBean(provider);
				beanFactory.initializeBean(provider, provider.getProviderClass());
				provider.init();
			} catch (Exception e) {
				logger.error("Error on setApplicationContext", e);
			}
		}
	}
	
}
