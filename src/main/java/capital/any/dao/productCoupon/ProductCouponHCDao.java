package capital.any.dao.productCoupon;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.hazelcast.core.IMap;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.processor.base.product.ProductCouponIsPaidEntryProcessor;
import capital.any.model.base.Product;
import capital.any.model.base.ProductCoupon;

/**
 * @author Eyal Goren
 *
 */
@Repository("ProductCouponHCDaoJOB")
public class ProductCouponHCDao extends capital.any.dao.base.productCoupon.ProductCouponHCDao  implements IProductCouponDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductCouponHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public boolean updateIsPaid(ProductCoupon productCoupon) {
		logger.info("ProductCouponHCDaoJOB; getCouponsToPay; ");
		Object o = null;
		IMap<Long, Product> products = (IMap<Long, Product>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCTS);
		if (products != null) {
			o = products.executeOnKey(productCoupon.getProductId(),
					new ProductCouponIsPaidEntryProcessor(productCoupon));
		}
		logger.info("after updateObservationLevel executeOnKey; ");
		return (o != null);
	}

	@Override
	@Deprecated
	public List<ProductCoupon> getCouponsToPay() {
		return null;
	}
	
}
