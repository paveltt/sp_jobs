package capital.any.dao.productCoupon;

import java.util.List;
import capital.any.model.base.ProductCoupon;

/**
 * @author Eyal Goren
 *
 */
public interface IProductCouponDao extends capital.any.dao.base.productCoupon.IProductCouponDao {
	
	public static final String GET_PRODUCT_COUPONS_TO_PAY = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_coupons pc " +
			" WHERE " +
			"	pc.payment_date < now() " +
			"   AND pc.is_paid = 0 ";
	
	public static final String UPDATE_PRODUCT_IS_PAID = 
			"UPDATE " +
			"	product_coupons " +
		    "SET " +
		    "	is_paid = :isPaid " +
		    "WHERE " + 
		    "	id = :id";
	
	
	/**
	 * get products coupons that need to check if we should pay or not
	 * @return
	 */
	List<ProductCoupon> getCouponsToPay();
	
	/**
	 * update the product coupon to be mark as processed
	 * @param productCoupon
	 * @return true id we update else false
	 */
	boolean updateIsPaid(ProductCoupon productCoupon);
}
