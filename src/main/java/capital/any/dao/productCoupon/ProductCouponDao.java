package capital.any.dao.productCoupon;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.dao.productCoupon.IProductCouponDao;
import capital.any.model.base.ProductCoupon;

/**
 * @author Eyal Goren
 *
 */
@Repository("ProductCouponDaoJob")
public class ProductCouponDao extends capital.any.dao.base.productCoupon.ProductCouponDao implements IProductCouponDao {

	@Override
	public List<ProductCoupon> getCouponsToPay() {
		return jdbcTemplate.query(GET_PRODUCT_COUPONS_TO_PAY, productCouponMapper);
	}

	@Override
	public boolean updateIsPaid(ProductCoupon productCoupon) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("isPaid", (productCoupon.isPaid() ? 1 : 0))
				.addValue("id", productCoupon.getId());
		return (jdbcTemplate.update(UPDATE_PRODUCT_IS_PAID, namedParameters) > 0);
	}		
}
