package capital.any.dao.product;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import capital.any.model.base.Product;

/**
 * @author EyalG
 *
 */
@Repository("ProductDaoJob")
public class ProductDao extends capital.any.dao.base.product.ProductDao implements IProductDao {
	
	@Override
	public List<Product> getProductsStatusSecondary() {
		return jdbcTemplate.query(SELECT_PRODUCTS_STATUSES_SECONDARY_STATE, productMapper);
	}
	
	@Override
	public List<Long> getProductsForSettle() {
		List<Long> list = jdbcTemplate.queryForList(GET_PRODUCTS_FOR_SETTLE, new HashMap<String, String>(), Long.class); 
		return list;
	}

	@Override
	public List<Product> getProductsStatusWaitingForSettle() {
		return jdbcTemplate.query(SELECT_PRODUCTS_STATUSES_WAITING_TO_SETTLE_STATE, productMapper);
	}
	
}
