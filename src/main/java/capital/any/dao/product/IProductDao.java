package capital.any.dao.product;

import java.util.List;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.model.base.Product;

/**
 * @author EyalG
 *
 */
public interface IProductDao extends capital.any.dao.base.product.IProductDao {
	
	public static final String UPDATE_PRODUCTS_STATUSES_SECONDARY_STATE = 
			" UPDATE " +
			" 	products " +
			" SET " +
			"   product_status_id = " + ProductStatusEnum.SECONDARY.getId() +
			" WHERE "	+ 
			"	product_status_id = " + ProductStatusEnum.SUBSCRIPTION.getId() +
			"	AND subscription_end_date < now() " +
			"	AND last_trading_date > now() ";
	
	public static final String SELECT_PRODUCTS_STATUSES_SECONDARY_STATE = 
			" SELECT " +
			" 	* " +
			" FROM " +
			" 	products " +
			" WHERE "	+ 
			"	product_status_id = " + ProductStatusEnum.SUBSCRIPTION.getId() +
			"	AND subscription_end_date < now() " +
			"	AND last_trading_date > now() ";
	
	public static final String UPDATE_PRODUCTS_STATUSES_WAITING_TO_SETTLE_STATE = 
			" UPDATE " +
			" 	products " +
			" SET " +
			"   product_status_id = " + ProductStatusEnum.WAITING_FOR_SETTLE.getId() +
			" WHERE "	+ 
			"	product_status_id = " + ProductStatusEnum.SECONDARY.getId() +
			"	AND last_trading_date <= now() " +
			"	AND redemption_date > now() ";
	
	public static final String SELECT_PRODUCTS_STATUSES_WAITING_TO_SETTLE_STATE = 
			" SELECT " +
			" 	* " +
			" FROM " +
			" 	products " +
			" WHERE "	+ 
			"	product_status_id = " + ProductStatusEnum.SECONDARY.getId() +
			"	AND last_trading_date <= now() " +
			"	AND redemption_date > now() ";
	
	public static final String GET_PRODUCTS_FOR_SETTLE = 
			" SELECT " +
			"	id " + 
			" FROM "	+ 
			"	products " +
			" WHERE " +
			"	redemption_date <= now() " +
			"	AND product_status_id = " + ProductStatusEnum.WAITING_FOR_SETTLE.getId();
	
	/**
	 * select all products that
	 * correct status is subscription 
	 * and we are after subscription_End_Date
	 * and we are before last_trading_date
	 * to status secondary
	 */
	List<Product> getProductsStatusSecondary();
	
	/**
	 * get all products id that need to be settled
	 * @return list of products id to settle
	 */
	List<Long> getProductsForSettle();

	/**
	 * select all products that
	 * correct status is secondary 
	 * and we are after last_trading_date
	 * and we are before redemption_date
	 * to status waitingforsettle
	 */
	List<Product> getProductsStatusWaitingForSettle();
}
