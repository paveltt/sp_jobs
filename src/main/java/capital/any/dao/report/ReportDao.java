package capital.any.dao.report;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author Eyal Goren
 *
 */
@Repository
public class ReportDao implements IReportDao {
	private static final Logger logger = LoggerFactory.getLogger(ReportDao.class);
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public Map<String, Object> getDailyReport() {
		
		return jdbcTemplate.queryForMap(SELECT_DAILY_REPORT, new HashMap<>());
	}	
}
