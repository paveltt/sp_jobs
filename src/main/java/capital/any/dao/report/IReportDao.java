package capital.any.dao.report;

import java.util.Map;

/**
 * @author eyal.goren
 *
 */
public interface IReportDao {
	
	public static final String SELECT_DAILY_REPORT = 
			" SELECT " +
				" * " +
			" FROM " +
				" daily_report ";
	
	
	
	/**
	 * select daily report
	 * @return map of column value
	 */
	Map<String, Object> getDailyReport();
}
