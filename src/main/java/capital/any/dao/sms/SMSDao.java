package capital.any.dao.sms;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.dao.base.sms.SMSMapper;
import capital.any.model.base.SMS;

/**
 * @author eran.levy
 *
 */
@Repository("SMSDaoJob")
public class SMSDao extends capital.any.dao.base.sms.SMSDao implements ISMSDao {
	
	@Autowired
	protected SMSMapper SMSMapper;  
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
		
	@Override
	public List<SMS> getSMStoSend(int limit) {	
		SqlParameterSource parameters = new MapSqlParameterSource("limit", limit);
		return jdbcTemplate.query(GET_SMS_TO_SEND, parameters, SMSMapper);		
	}
	
	
}
