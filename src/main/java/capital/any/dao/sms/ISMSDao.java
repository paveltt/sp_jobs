package capital.any.dao.sms;

import java.util.List;
import capital.any.model.base.SMS;

/**
 * @author eran.levy
 *
 */
public interface ISMSDao extends capital.any.dao.base.sms.ISMSDao {
	
	public static final String GET_SMS_TO_SEND = 
			" SELECT " +
			" 	* " +
			" FROM " +
			" 	sms s " +
			" WHERE "	+ 
			"	( s.status_id =   " + SMS.Status.QUEUED.getToken() + " AND time_scheduled <= now() ) " +
			"	OR (s.status_id =" + SMS.Status.NOT_VERIFIED.getToken() + " ) " +
			" ORDER BY " +
            "	time_scheduled, " +
            "	id " +			
			" LIMIT " +
			" 	:limit ";
	
	/**
	 * @return
	 */
	List<SMS> getSMStoSend(int limit);

}
