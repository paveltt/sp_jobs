package capital.any.dao.email;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Email;

/**
 * @author Eyal Goren
 *
 */
@Repository("EmailDaoJOB")
public class EmailDao extends capital.any.dao.base.email.EmailDao implements IEmailDao {
	private static final Logger logger = LoggerFactory.getLogger(EmailDao.class);
	
	@Autowired
	private EmailMapper emailMapper;

	@Override
	public List<Email> selectQueued() {
		return jdbcTemplate.query(SELECT_QUEUED, emailMapper);
	}

	@Override
	public boolean updateStatus(Email email) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", email.getId())
				.addValue("statusId", email.getStatusId());
		return (jdbcTemplate.update(UPDATE_STATUS, namedParameters) > 0);
	}

	@Override
	public boolean updateSend(Email email) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", email.getId());
		return (jdbcTemplate.update(UPDATE_SENT, namedParameters) > 0);
	}

	@Override
	public boolean updateRetries(Email email) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", email.getId());
		return (jdbcTemplate.update(UPDATE_RETRIES, namedParameters) > 0);
	}		
}
