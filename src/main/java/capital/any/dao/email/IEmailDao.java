package capital.any.dao.email;

import java.util.List;

import capital.any.base.enums.EmailStatus;
import capital.any.model.base.Email;

/**
 * @author eyal.goren
 *
 */
public interface IEmailDao extends capital.any.dao.base.email.IEmailDao {
	
	public static final String SELECT_QUEUED = 
			" SELECT " +
			"	* " +
			" FROM " +
			"	emails " +
			" WHERE " +
			"	status_id = " + EmailStatus.QUEUED.getStatusId();
	
	public static final String UPDATE_STATUS = 
			" UPDATE " +
			" 	emails " +
			" SET " +
			"   status_id = :statusId " +
			" WHERE "	+ 
			"	id = :id ";
	
	public static final String UPDATE_SENT = 
			" UPDATE " +
			" 	emails " +
			" SET " +
			"   status_id = " + EmailStatus.SENT.getStatusId() + ", " +
			"   time_sent = CURRENT_TIMESTAMP(3) " +
			" WHERE "	+ 
			"	id = :id ";
	
	public static final String UPDATE_RETRIES = 
			" UPDATE " +
			" 	emails " +
			" SET " +
			"   status_id = " + EmailStatus.QUEUED.getStatusId() + ", " +
			"   retries = retries + 1 " +
			" WHERE "	+ 
			"	id = :id ";
	
	/**
	 * select all email in queued 
	 * @return {@link}List<Email>
	 */
	List<Email> selectQueued();
	
	/**
	 * update email status by id
	 * @param email
	 * @return true if success else false
	 */
	boolean updateStatus(Email email);
	
	/**
	 * update that email send successful
	 * @param email
	 * @return true if success else false
	 */
	boolean updateSend(Email email);
	
	/**
	 * update that email not send and need to retry
	 * @param email
	 * @return true if success else false
	 */
	boolean updateRetries(Email email);
}
