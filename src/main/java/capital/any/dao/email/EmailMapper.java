package capital.any.dao.email;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.base.enums.ActionSource;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.Email;

/**
 * @author Eyal Goren
 *
 */
@Component
public class EmailMapper extends MapperBase implements RowMapper<Email> {
	
	@Override
	public Email mapRow(ResultSet rs, int row) throws SQLException {		
		Email email = new Email();
		email.setId(rs.getInt("id"));
		email.setStatusId(rs.getInt("status_id"));
		email.setActionSource(ActionSource.get(rs.getInt("action_source_id")));
		email.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		email.setEmailInfo(rs.getString("email_info"));
		email.setRetries(rs.getInt("retries"));
		email.setTimeSent(convertTimestampToDate(rs.getTimestamp("time_sent")));
		return email;
	}
	
	public Email mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}			
}
	

