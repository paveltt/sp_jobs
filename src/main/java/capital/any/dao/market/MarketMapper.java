package capital.any.dao.market;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.MarketAO;

/**
 * @author Eyal Goren
 *
 */
@Component("MarketMapperJOB")
public class MarketMapper extends MapperBase implements RowMapper<MarketAO> {
	
	@Override
	public MarketAO mapRow(ResultSet rs, int row) throws SQLException {		
		MarketAO market = new MarketAO();
		market.setId(rs.getInt("id"));
		market.setClosingLevel(rs.getDouble("closing_level"));
		market.setName(rs.getString("name"));
		market.setTimeEstCloseing(convertTimestampToDate(rs.getTimestamp("time_est_closing")));
		return market;
	}
	
	public MarketAO mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}			
}
	

