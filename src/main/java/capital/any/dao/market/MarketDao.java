package capital.any.dao.market;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.MarketAO;

/**
 * @author Eyal Goren
 *
 */
@Repository("MarketDaoJOB")
public class MarketDao extends capital.any.dao.base.market.MarketDao implements IMarketDao {
	private static final Logger logger = LoggerFactory.getLogger(MarketDao.class);
	
	@Autowired
	@Qualifier("MarketMapperJOB")
	private MarketMapper emailMapper;
//	@Autowired
//	@Qualifier("oracleJdbc")
//	private NamedParameterJdbcTemplate jdbcTemplateOracle;

	@Override
	public List<MarketAO> getHistoryAO(int startDay, int endDay, Integer marketId) {
//		SqlParameterSource parameters = new MapSqlParameterSource
//				 ("startDay", startDay)
//		.addValue("endDay", endDay)
//		.addValue("marketId", marketId);
//		return jdbcTemplateOracle.query(SELECT_MARKETS_AO, parameters, emailMapper);
		return null;
	}

	@Override
	public List<MarketAO> getHistoryAOYearly(Integer marketId) {
//		SqlParameterSource parameters = new MapSqlParameterSource("marketId", marketId);
//		return jdbcTemplateOracle.query(SELECT_AO_HISTORY_YEARLY, parameters, emailMapper);
		return null;
	}

	
}
