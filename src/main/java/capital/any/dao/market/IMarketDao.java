package capital.any.dao.market;

import java.util.List;

import capital.any.model.MarketAO;

/**
 * @author eyal.goren
 *
 */
public interface IMarketDao extends capital.any.dao.base.market.IMarketDao {
	
	public static final String SELECT_MARKETS_AO = 
			" SELECT " +
			"	* " +
			" FROM " +
			"	etrader.ac_eod_markets m " +
			" WHERE " +
			"   m.id = :marketId " +
			"	AND m.time_est_closing >= sysdate - :startDay " +
			"   AND m.time_est_closing <= sysdate - :endDay ";
	

	public static final String SELECT_AO_HISTORY_YEARLY = 
			" SELECT " +
			"	* " +
			" FROM " +
			"	etrader.ac_eod_markets m " +
			" WHERE " +
			"   m.id = :marketId " +
			"	AND m.time_est_closing >= TRUNC(sysdate - 365) " +
			"   AND m.time_est_closing < TRUNC(sysdate) ";
	
	/**
	 * get history from ao for specific market and days
	 * @param startDay how many days to reduce from today (365 for 1 year)
	 * @param endDay how many days to reduce from today (365 for 1 year)
	 * @param marketId market id in AO
	 */
	List<MarketAO> getHistoryAO(int startDay, int endDay, Integer marketId);
	
	/**
	 * get history of last year
	 * @param marketId
	 * @return list of daily closing level of last year
	 */
	List<MarketAO> getHistoryAOYearly(Integer marketId);
}
