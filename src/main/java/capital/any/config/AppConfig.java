package capital.any.config;

import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import capital.any.job.IJob;
import capital.any.model.base.Job;
import capital.any.service.base.job.IJobService;

/**
 * @author eranl
 *
 */
@Configuration				
public class AppConfig implements SchedulingConfigurer, ApplicationContextAware {
	
	private static final Logger logger = LoggerFactory.getLogger(AppConfig.class);
	
	@Autowired
	private IJobService jobService;		
	private HashMap<Integer, Job> jobs; 
			
	@Primary
	@Bean(name="mySQLDS")
	@ConfigurationProperties(prefix="spring.datasource")
	public DataSource mySQLDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Primary
	@Bean(name="mySQLJdbc")
	public NamedParameterJdbcTemplate jdbcTemplateMySQL(@Qualifier("mySQLDS") DataSource dMySQL) {
		return new NamedParameterJdbcTemplate(dMySQL);
	}
		
//	@Bean(name="oracleDS")
//	@ConfigurationProperties(prefix="spring.secondDatasource")
//	public DataSource oracleDataSource() {
//	    return DataSourceBuilder.create().build();
//	}
//		
//	@Bean(name="oracleJdbc")
//	public NamedParameterJdbcTemplate jdbcTemplateOracle(@Qualifier("oracleDS") DataSource tOracle) {
//		return new NamedParameterJdbcTemplate(tOracle);
//	}
				
	@Bean
	public HashMap<Integer, IJob> getHM() {
		return new HashMap<Integer, IJob>();
	}
			
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
    	logger.debug("Tasks job service has been started");
    	for (Integer jobId : getHM().keySet()) {
	    	taskRegistrar.setScheduler(taskScheduler());
	        taskRegistrar.addTriggerTask(
	                    new Runnable() {
	                        public void run() {
	                        	try {                        		                        		
	                        		getHM().get(jobId).run();                              		
								} catch (Exception e) {
									logger.error("Error while trying to run job ", e);
								} finally {
									jobService.updateJobById(jobId);
	                        	}
	                        }
	                    }, new CronTrigger(jobs.get(jobId).getCronExpression())
	                );
    	}
    }

    @Bean(destroyMethod="shutdown")
    public Executor taskScheduler() {
    	logger.debug("Tasks job service has been shutdown");
        return Executors.newScheduledThreadPool(42);
    }

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		jobs = jobService.getJobs();
		for (Job job : jobs.values()) {
	        try {
				Class<?> c = Class.forName(job.getJobClass());
				IJob currentJob = (IJob) c.newInstance();
		        beanFactory.autowireBean(currentJob);
		        beanFactory.initializeBean(currentJob, job.getJobClass());
		        getHM().put(job.getId(), currentJob);
			} catch (Exception e) {
				logger.error("Error on setApplicationContext", e);
			}
		}
	}
	        
    
}
