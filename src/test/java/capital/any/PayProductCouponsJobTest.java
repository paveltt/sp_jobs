package capital.any;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.model.base.ProductCoupon;
import capital.any.service.productCoupon.IProductCouponService;

@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class PayProductCouponsJobTest {
	private static final Logger logger = LoggerFactory.getLogger(PayProductCouponsJobTest.class);
	
	@Autowired
	@Qualifier("ProductCouponServiceJob")
	private IProductCouponService productCouponService;
	
	@Test
	@Transactional
	@Ignore
	public void payCouponTest() {
		List<ProductCoupon> productCoupons = productCouponService.getProductCoupons(1400);
		try {
			productCouponService.pay(productCoupons.get(0));
		} catch (Exception e) {
			logger.error("", e);
		}
	}

}
