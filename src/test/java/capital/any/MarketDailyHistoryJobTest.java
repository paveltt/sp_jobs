package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.job.MarketDailyHistoryJob;

@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class MarketDailyHistoryJobTest {
		
	@Autowired
	MarketDailyHistoryJob marketDailyHistoryJob;
	
	@Test
	@Transactional
	public void runMarketDailyHistoryJob() {
		marketDailyHistoryJob.run();
	}

}
