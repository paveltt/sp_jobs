package capital.any;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.ActionSource;
import capital.any.model.base.ProductHistory;
import capital.any.service.investment.IInvestmentService;
import capital.any.service.product.IProductService;

@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class ProductsStatusesJobTest {
	private static final Logger logger = LoggerFactory.getLogger(ProductsStatusesJobTest.class);
	
	@Autowired
	@Qualifier("InvestmentServiceJOB")
	private IInvestmentService investmentService;
	
	@Autowired 
	@Qualifier("ProductServiceJob")
	private IProductService productService;
	
	@Test
	@Transactional
	@Ignore
	public void changeProductStatusTest() {
		try {
			productService.changeProductStatus();
		} catch (Exception e) {
			logger.error("", e);
		}
	}
	
	@Test
	@Transactional
	@Ignore
	public void cancelPendingInvestmentsTest() {
		try {
			investmentService.cancelPendingInvestments(1292);
		} catch (Exception e) {
			logger.error("", e);
		}
	}

}
