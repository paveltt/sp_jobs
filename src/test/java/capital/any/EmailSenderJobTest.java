package capital.any;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.job.EmailSenderJob;
import capital.any.model.base.Email;
import capital.any.service.email.IEmailService;

@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class EmailSenderJobTest {
	private static final Logger logger = LoggerFactory.getLogger(EmailSenderJobTest.class);
	
	@Autowired 
	@Qualifier("EmailServiceJOB")
	private IEmailService emailService;
	
	@Autowired
	EmailSenderJob emailSenderJob;
	
	@Test
	@Transactional
	public void selectQueued() {
		try {
			List<Email> selectQueued = emailService.selectQueued();
			for (Email email : selectQueued) {
				logger.debug(email.toString());
			}
		} catch (Exception e) {
			logger.error("cant select queued emails", e);
		}
	}
	
	@Test
	@Transactional
	public void testJob() throws Exception {
		emailSenderJob.run();
	}

}
