package capital.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.base.enums.EmailAction;
import capital.any.base.enums.EmailParameters;
import capital.any.base.enums.EmailType;
import capital.any.model.base.MailInfo;
import capital.any.model.base.Sendgrid;
import capital.any.model.base.SendgridRecipient;
import capital.any.service.sendgrid.ISendgridService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class SendgridServiceTest {
	
	@Autowired
	private ISendgridService sendgridService;
	
	@Test
	public void sendMail() {
		Map<String, String> map = new HashMap<String, String>();
		map.put(EmailParameters.PARAM_FIRST_NAME.getParam(), "Test first name");
		map.put(EmailParameters.USER_NAME.getParam(), "Test user name");
		MailInfo mailInfo = new MailInfo(
				"eyal.ohana@anyoption.com",
				null,
				map,
				EmailAction.FORGET_PASSWORD.getId(),
				EmailType.TEMPLATE.getId(),
				2);
		try {
			sendgridService.sendMail(mailInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void sendCampaign() {
		Sendgrid sendgrid = new Sendgrid();
		sendgrid.setCampaignId("837571");
		sendgridService.sendCampaign(sendgrid);
	}
	
	@Test
	public void updateRecipients() {
		SendgridRecipient sendgridRecipient = new SendgridRecipient();
		sendgridRecipient.setEmail("eyal.ohana@anyoption.com");
		sendgridRecipient.setAcUserId(123);
		List<SendgridRecipient> list = new ArrayList<SendgridRecipient>();
		list.add(sendgridRecipient);
		Sendgrid sendgrid = new Sendgrid();
		sendgrid.setSendgridRecipientList(list);
		sendgridService.updateRecipients(sendgrid);
	}
}
