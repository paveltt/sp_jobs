package capital.any;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.base.enums.EmailParameters;
import capital.any.model.base.MailInfo;
import capital.any.service.mail.IMailService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class MailServiceTest {
	
	@Autowired
	private IMailService mailService ;
	
	@Test
	public void prepareAndSend() {
		/* Content's parameter */
		Map<String, String> map = new HashMap<String, String>();
		map.put(EmailParameters.PARAM_FIRST_NAME.getParam(), "anycapital");
		/* Mail Info */
		MailInfo mailInfo = new MailInfo();
//		mailInfo.setFrom("support.test@anyoption.com");
		mailInfo.setTo("eyal.goren@anycapital.com");
		mailInfo.setSubject("Test AC");
		mailInfo.setTemplateName("mailTemplateTest");
		mailInfo.setMap(map);
		mailInfo.setTypeId(1);
		/* Prepare and send */
		mailService.prepareAndSend(mailInfo);
	}
	
}
