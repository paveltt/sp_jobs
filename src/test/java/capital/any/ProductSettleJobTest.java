package capital.any;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.base.enums.ActionSource;
import capital.any.model.base.ProductHistory;
import capital.any.service.product.IProductService;

@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class ProductSettleJobTest {
	private static final Logger logger = LoggerFactory.getLogger(ProductSettleJobTest.class);
	
	@Autowired 
	@Qualifier("ProductServiceJob")
	private IProductService productService;
	
	@Test
	@Transactional
	@Ignore
	public void settleTest() {
		try {
			productService.settle(1273, new ProductHistory(ActionSource.JOB));
		} catch (Exception e) {
			logger.error("", e);
		}
	}

}
