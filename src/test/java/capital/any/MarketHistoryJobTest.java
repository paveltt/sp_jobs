package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.job.MarketHistoryJob;

@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class MarketHistoryJobTest {
		
	@Autowired
	MarketHistoryJob marketHistoryJob;
	
	@Test
	@Transactional
	public void runMarketHistoryJob() {
		marketHistoryJob.run();
	}

}
