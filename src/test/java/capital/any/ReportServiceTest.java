package capital.any;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.base.enums.EmailAction;
import capital.any.base.enums.EmailGroup;
import capital.any.base.enums.EmailType;
import capital.any.model.base.Email;
import capital.any.model.base.MailInfo;
import capital.any.service.base.email.IEmailService;
import capital.any.service.base.emailGroup.IEmailGroupService;
import capital.any.service.report.IReportService;
import capital.any.service.report.ReportService;

/**
 * 
 * @author eyal.Goren
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ReportServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(ReportService.class);
	
	@Autowired
	private IReportService reportService ;
	
	@Autowired
	private IEmailService emailService;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private IEmailGroupService emailGroupService;
	
	@Test
	public void getDailyReport() {
		StringBuilder email = new StringBuilder();
		email.append("<html><head><style type='text/css'>table, th, td {border: 1px solid black;}"
				+ "</style></head>"
                + "<body>");
		email.append(//"<b>product that move to status secondary</b>"
                "<table class='out'><span style=border-color: black, border-width: 1px>");
		Map<String, Object> dailyReport = reportService.getDailyReport();
		dailyReport.forEach((k,v)->{
			email.append("<tr><td>" + k + "</td><td>" + (v == null ? "0" : v.toString()) + " </td></tr>");
		});
		email.append("</table>");
		email.append("</body></html>");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DAY_OF_MONTH, -1);
		String subject = "Daily Report for " + sdf.format(yesterday.getTime());
		MailInfo mailInfo = new MailInfo(
				emailGroupService.get(EmailGroup.DAILY_REPORT.getId()),
				//"eyal.goren@anycapital.com",
				subject, 
				email.toString(), 
				EmailType.NO_TEMPLATE_TEXT_HTML.getId(), 
				EmailAction.CHANGE_PRODUCT_STATUS.getId());
		try {
			Email emailToSend = new Email();		
			emailToSend.setActionSource(ActionSource.JOB);
			emailToSend.setEmailInfo(mapper.writeValueAsString(mailInfo));
			emailService.insert(emailToSend);
		} catch (JsonProcessingException e) {
			logger.error("cant send email", e);
		}
		logger.debug("report " + dailyReport);
	}
	
}
