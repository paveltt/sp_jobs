package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.job.SMSJob;

/**
 * @author eran.levy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class SMSJobTest {
	
	@Autowired
	SMSJob SMSJob;
	
	@Test
	@Transactional
	public void runSMSJob() {
		//SMSJob.run();
	}
}
