package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.job.MarketJob;

@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class MarketJobTest {
		
	@Autowired
	MarketJob marketJob;
	
	@Test
	@Transactional
	public void runMarketHistoryJob() throws Exception {
		marketJob.run();
	}

}
