package capital.any;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.model.MarketAO;
import capital.any.service.market.IMarketService;

/**
 * 
 * @author Eyal.G
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class MarketServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(MarketServiceTest.class);
	
	@Autowired
	private IMarketService marketService ;
	
	@Test
	public void getHistoryAO() {
		logger.debug("get history start");
		List<MarketAO> historyAO = marketService.getHistoryAO(8, 1, 3);
		for (MarketAO marketAO : historyAO) {
			logger.debug("market " + marketAO);
		}
		logger.debug("get history end");
	}
	
	@Test
	public void getHistoryAOYearly() {
		logger.debug("get History AO Yearly");
		List<MarketAO> historyAO = marketService.getHistoryAOYearly(3);
		for (MarketAO marketAO : historyAO) {
			logger.debug("market " + marketAO);
		}
		logger.debug("get history end");
	}
	
}
